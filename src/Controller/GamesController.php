<?php

namespace App\Controller;

use App\Entity\Games;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\GameFormType;

class GamesController extends AbstractController
{
    /**
     * @Route("/games", name="games")
     */
    public function index(): Response
    {
        $Games = new Games();
        $form = $this->createForm(GameFormType::class, $Games);

        return $this->render('games/index.html.twig', [
            'controller_name' => 'GamesController',
            'gameForm' => $form->createView(),
        ]);
    }
}
