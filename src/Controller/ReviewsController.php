<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\ReviewsFormType;
use App\Form\Type\TaskType;


class ReviewsController extends AbstractController
{
    /**
     * @Route("/review", name="reviews")
     */
    public function new(Request $request): Response
    {

        $user = new User();
        $form = $this->createForm(ReviewsFormType::class, $user);

        return $this->render('reviews/index.html.twig', [
            'ReviewsForm' => $form->createView(),
        ]);
    }
    public function index(): Response
    {
        return $this->render('reviews/index.html.twig', [
            'controller_name' => 'ReviewsController',
        ]);
    }
}


// $form->handleRequest($request);

// $form->isValid() && $form->isSubmitted()  daarna dit in een if