<?php

namespace App\Entity;

use App\Repository\GamesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GamesRepository::class)
 */
class Games
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=125)
     */
    private $gameName;

    /**
     * @ORM\Column(type="integer")
     */
    private $gameID;


    /**
     * @ORM\Column(type="string", length=100)
     */
    private $gameImg;

    /**
     * @ORM\Column(type="text")
     */
    private $gameDescription;

    /**
     * @ORM\Column(type="float")
     */
    private $gamePrice;

    /**
     * @ORM\ManyToOne(targetEntity=Console::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $console;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameName(): ?string
    {
        return $this->gameName;
    }

    public function setGameName(string $gameName): self
    {
        $this->gameName = $gameName;

        return $this;
    }

    public function getGameID(): ?int
    {
        return $this->gameID;
    }

    public function setGameID(int $gameID): self
    {
        $this->gameID = $gameID;

        return $this;
    }



    public function getGameImg(): ?string
    {
        return $this->gameImg;
    }

    public function setGameImg(string $gameImg): self
    {
        $this->gameImg = $gameImg;

        return $this;
    }

    public function getGameDescription(): ?string
    {
        return $this->gameDescription;
    }

    public function setGameDescription(string $gameDescription): self
    {
        $this->gameDescription = $gameDescription;

        return $this;
    }

    public function getGamePrice(): ?float
    {
        return $this->gamePrice;
    }

    public function setGamePrice(float $gamePrice): self
    {
        $this->gamePrice = $gamePrice;

        return $this;
    }

    public function getConsole(): ?Console
    {
        return $this->console;
    }

    public function setConsole(?Console $console): self
    {
        $this->console = $console;

        return $this;
    }
}
