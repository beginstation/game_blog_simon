<?php

namespace App\Entity;

use App\Repository\ReviewsTableRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReviewsTableRepository::class)
 */
class ReviewsTable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $reviewID;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $reviewTitle;

    /**
     * @ORM\Column(type="text")
     */
    private $reviewText;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReviewID(): ?int
    {
        return $this->reviewID;
    }

    public function setReviewID(int $reviewID): self
    {
        $this->reviewID = $reviewID;

        return $this;
    }

    public function getReviewTitle(): ?string
    {
        return $this->reviewTitle;
    }

    public function setReviewTitle(string $reviewTitle): self
    {
        $this->reviewTitle = $reviewTitle;

        return $this;
    }

    public function getReviewText(): ?string
    {
        return $this->reviewText;
    }

    public function setReviewText(string $reviewText): self
    {
        $this->reviewText = $reviewText;

        return $this;
    }
}
