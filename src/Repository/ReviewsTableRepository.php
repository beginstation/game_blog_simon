<?php

namespace App\Repository;

use App\Entity\ReviewsTable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReviewsTable|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReviewsTable|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReviewsTable[]    findAll()
 * @method ReviewsTable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewsTableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReviewsTable::class);
    }

    // /**
    //  * @return ReviewsTable[] Returns an array of ReviewsTable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReviewsTable
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
